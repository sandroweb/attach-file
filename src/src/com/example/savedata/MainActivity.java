package com.example.savedata;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {

	private String filename = "MySampleFile.txt";
	private String filepath = "MyFileStorage";
	File myInternalFile;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ContextWrapper contextWrapper = new ContextWrapper(
				getApplicationContext());
		File directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);
		myInternalFile = new File(directory, filename);
	}

	public void myClick(View v) {
		// comprobarSDCard(this);

		createFile("conte�do do arquivo");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void createFile(String sBody) {

		String myData = "novo texto la l� lah";

		try {
			FileOutputStream fos = new FileOutputStream(myInternalFile);
			fos.write(myData.getBytes());
			fos.close();
			sendMail(myInternalFile);
			Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			e.printStackTrace();
			Toast.makeText(this, "Save error", Toast.LENGTH_SHORT).show();
		}

		/*
		 * ContextWrapper contextWrapper = new ContextWrapper(
		 * getApplicationContext()); File directory =
		 * contextWrapper.getDir(filepath, Context.MODE_PRIVATE); myInternalFile
		 * = new File(directory, filename);
		 */
		
		/*try {
			FileInputStream fis = new FileInputStream(myInternalFile);
			DataInputStream in = new DataInputStream(fis);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				myData = myData + strLine;
			}
			sendMail(Uri.fromFile(myInternalFile));
			in.close();
			Toast.makeText(this, "Loaded", Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			e.printStackTrace();
			Toast.makeText(this, "Load error", Toast.LENGTH_SHORT).show(); }
		 */

		/*
		 * Intent sendIntent = new Intent(Intent.ACTION_SEND);
		 * sendIntent.setType("text/plain");
		 * sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {
		 * "sandro@box3.com.br" }); sendIntent.putExtra(Intent.EXTRA_STREAM,
		 * Uri.fromFile(myInternalFile));
		 * 
		 * try { startActivity(Intent.createChooser(sendIntent,
		 * "Enviar email...")); } catch
		 * (android.content.ActivityNotFoundException ex) {
		 * Toast.makeText(getBaseContext(),
		 * "There are no email clients installed.", Toast.LENGTH_SHORT) .show();
		 * }
		 */

		/*
		 * try { FileInputStream fis = new FileInputStream(myInternalFile);
		 * DataInputStream in = new DataInputStream(fis); BufferedReader br =
		 * new BufferedReader(new InputStreamReader(in)); String strLine; while
		 * ((strLine = br.readLine()) != null) { myData = myData + strLine; }
		 * in.close(); Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
		 * } catch (IOException e) { e.printStackTrace(); Toast.makeText(this,
		 * "Error: ", Toast.LENGTH_SHORT).show(); }
		 */
	}


	public void sendMail(File file) throws IOException {
		
		FileInputStream fis = new FileInputStream(file);
		DataInputStream in = new DataInputStream(fis);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		
		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		sendIntent.setClassName("com.google.android.gm",
				"com.google.android.gm.Compo");
		sendIntent.setType("text/richtext");
		sendIntent.putExtra(Intent.EXTRA_EMAIL,
				new String[] { "sandro@box3.com.br" });
		// sendIntent.putExtra(Intent.EXTRA_STREAM,
		// Uri.fromFile(myInternalFile));
		

		while ((strLine = br.readLine()) != null) {
			sendIntent.putExtra(Intent.EXTRA_TEXT, strLine);
		}

		in.close();
		
		sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));

		try {
			startActivity(Intent.createChooser(sendIntent, "Enviar email..."));
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(getBaseContext(),
					"There are no email clients installed.", Toast.LENGTH_SHORT)
					.show();
		}
	}
}
